# Qwark

```js
const Qwark = require("qwark");
new Qwark()
  .get("/", (req, res) => {
    res.send("Hello World");
  })
  .listen(3000);
```

## Usage

```bash
$ git clone https://bitbucket.org/ElGreetly/qwark
$ cd qwark
$ npm i
```

- Import the index from qwark directory to your project

```js
const Qwark = require("../qwark");
```

- Create app

```js
const app = new Qwark();
```

- Start adding your route

```js
app.get("/my/route", (req, res) => {
  res.end("Great");
});
app.post("/my/post", (req, res) => {
  res.end("Nice");
});
```

- Also you can add middleware

```js
app.use("/middleware", (req, res) => {
  req.middleware = true;
});
```

- Start server listening

```js
app.listen(3000);
```

### Notes

- next method not implemented yet for escaping middleware
- paths with keys not tested
- query params still not implemented
- URI Decoding is not used yet

## How it works

![Router Composition](./imgs/composite_route.png)

![Router Tree](./imgs/router.png)
