const Qwark = require('../../')

new Qwark()
  .get('/', (req, res) => res.end('Hello World'))
  .listen(3000)
