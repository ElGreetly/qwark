const logger = require('./logger')

/**
 * @class AppError
 *
 * Application Error
 */
class AppError extends Error {
  /**
    * @constructor
    *
    * @param {String} msg
    * @param {String} statusMsg
    * @param {Number} code
    */
  constructor (msg, statusMsg, code) {
    super(msg)
    this.statusMsg = statusMsg || null
    this.msg = msg || ''
    this.code = code || null
    logger.error(msg)
  }
}

module.exports = AppError
