const http = require('http')
const Router = require('./router')
const Request = require('./request')

/**
 * @class Qwark
 *
 * The server
 */
class Qwark {
  /**
   * @constructor
   */
  constructor () {
    this.router = new Router('/')

    this.handler = async (req, res) => {
      const request = new Request(req, this.router)
      await request.handleRequest(req, res)
    }

    this.handler.router = this.router

    /**
     * Adds middleware to a route
     *
     * @param {String} path
     * @param {Function} fn
     */
    this.handler.use = (path, fn) => {
      this.handler.router.use(path, fn)
      return this.handler
    }

    /**
     * Adds new post method with a given route
     * It could work with Builder design
     *
     * @param {String} path
     * @param {Function} fn
     */
    this.handler.post = (path, fn) => {
      this.handler.router.post(path, fn)
      return this.handler
    }

    /**
     * Starts server listening on the given port
     *
     * @param {Number} port
     * @param {Function} cb
     */
    this.handler.listen = (port, cb) => {
      this.server = http.createServer(async (req, res) => {
        await this.handler(req, res)
      })
      this.server.listen(port, cb)
    }

    /**
     * Adds new get method with a given route
     * It could work with Builder design
     *
     * @param {String} path
     * @param {Function} fn
     */
    this.handler.get = (path, fn) => {
      this.handler.router.get(path, fn)
      return this.handler
    }

    return this.handler
  }
}

module.exports = Qwark
