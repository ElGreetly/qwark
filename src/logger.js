/**
 * @class Logger
 *
 * Colored Logs
 */
class Logger {
  /**
   * Info stdout logs
   *
   * @param {String} msg
   */
  static info (msg) {
    Logger.log(msg, 'info', 32)
  }

  /**
   * Error stderr logs
   *
   * @param {String} msg
   */
  static error (msg) {
    Logger.log(msg, 'error', 31)
  }

  /**
   * Print out logs to the terminal
   *
   * @param {String} msg
   * @param {error|info} level
   * @param {Number} colorCode
   */
  static log (msg, level, colorCode) {
    console[level](`\x1b[1;${colorCode}m${msg}\x1b[0m`)
  }
}

module.exports = Logger
