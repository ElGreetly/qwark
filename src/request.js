const Response = require('./response')

/**
 * @class Request
 *
 * This class for handling Requests
 */
class Request {
  /**
    * @constructor
    *
    * @param {HTTPRequest} req
    * @param {Router} router
    */
  constructor (req, router) {
    req.routes = []
    req.params = {}
    this.router = router
  }

  /**
   *
   *
   * @param {HTTPRequest} req
   * @param {HTTPResponse} res
   */
  async handleRequest (req, res) {
    const response = new Response(res)
    const isAvailable = this.router.find(req.url, req)
    if (!isAvailable) {
      return response.notFound(res)
    }
    for (let i = 0; i < req.routes.length; i++) {
      for (let x = 0; x < req.routes[i].stack.length; x++) {
        await req.routes[i].stack[x](req, res)
      }
    }
  }
}

module.exports = Request
