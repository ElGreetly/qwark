/**
 * @class Response
 *
 * This class for handling Responses
 */
class Response {
  /**
   * @constructor
   *
   * @param {HTTPResponse} res
   */
  constructor (res) {
    res.setHeader('x-powered-by', 'Qwark')
  }

  /**
   * Send not found response, usually used when route is not found
   *
   * @param {HTTPResponse} res
   */
  notFound (res) {
    res.writeHead(404, 'Not Found')
    res.end('Not Found')
  }
}

module.exports = Response
