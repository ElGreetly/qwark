const Route = require('./route')
const Method = require('./method')

/**
 * @class Method
 *
 * This is a composite class, has the responsibility for the routing
 */
class Router extends Route {
  /**
    * @constructor
    *
    * @param {String} path
    * @param {Function} fn
    */
  constructor (path, method, fn) {
    super(path)
    const remainPath = path.substr(this.name.length)

    if (typeof fn === 'function' && !remainPath && !method) {
      this.stack.push(fn)
    }
    // Build the composite tree for the path
    if (remainPath) this.children.push(new Router(remainPath, method, fn))
    if (!remainPath && method) this.children.push(new Method(method, fn, this))
  }

  /**
   * @override
   * Adds new routes recursively
   *
   * @param {String} path
   * @param {Function} fn
   * @param {String} method
   */
  add (path, method, fn) {
    const { lastRoute: parentRoute, updatedPath } = this.findLastRoute(path, method)
    if (!updatedPath && method) {
      parentRoute.children.push(new Method(method, fn, parentRoute))
      return parentRoute
    }
    parentRoute.children.push(new Router(updatedPath, method, fn))
    return this
  }

  /**
   * @override
   * Finds request true routes
   *
   * @param {String} path
   * @param {HTTPRequest} req
   */
  find (path, req) {
    if (path === '/' && req.url === path) {
      const route = this.children.find(r => req.method.toLowerCase() === r.method)
      if (route) {
        req.routes.push(this, route)
      }
      return !!route
    }
    const routeName = this.subCurrentRouteName(path)
    const route = this.children.find(route => route.path.exec(routeName))
    if (this.keys) {
      req.params[this.keys[0]] = routeName.substr(1)
    }
    req.routes.push(this)
    if (!route) return false
    return route.find(path.substr(routeName.length || 1), req)
  }

  /**
   * Adds middleware to a route
   *
   * @param {String} path
   * @param {Function} fn
   */
  use (path, fn) {
    this.add(path, null, fn)
  }

  /**
   * Adds new post method with a given route
   * It could work with Builder design
   *
   * @param {String} path
   * @param {Function} fn
   */
  post (path, fn) {
    this.add(path, 'post', fn)
    return this
  }

  /**
   * Adds new get method with a given route
   * It could work with Builder design
   *
   * @param {String} path
   * @param {Function} fn
   */
  get (path, fn) {
    this.add(path, 'get', fn)
    return this
  }
}

module.exports = Router
