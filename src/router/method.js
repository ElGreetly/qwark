const Route = require('./route')
const AppError = require('../appError')

/**
 * @class Method
 *
 * This is the route leaf, with the routes method data
 *
 */
class Method extends Route {
  /**
    *
    * @param {String} method
    * @param {Function} fn
    * @param {Router} parent
    */
  constructor (method, fn, parent) {
    super('')
    if (typeof fn !== 'function') {
      throw new AppError('Route callback should be function')
    }
    this.name = ''
    this.path = /^$/
    this.method = method
    this.parent = parent
    this.stack.push(fn)
  }

  /**
   * @override
   * Finds request true method
   *
   * @param {String} path
   * @param {HTTPRequest} req
   */
  find (path, req) {
    if (this.method === req.method.toLowerCase() && path === '') {
      req.routes.push(this)
      return true
    }
    return false
  }

  /**
   * @override
   *
   * @returns {Router}
   */
  add () {
    return this.parent
  }
}

module.exports = Method
