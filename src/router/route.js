const { pathToRegexp } = require('path-to-regexp')

/**
 * @class Route
 *
 * The main component for the router
 */
class Route {
  /**
   * @constructor
   *
   * @param {String} path
   */
  constructor (path) {
    this.keys = null
    this.stack = []
    this.method = null
    this.allowedMethods = ['post', 'get']
    this.children = []
    this.name = this.subCurrentRouteName(path)
    this.path = pathToRegexp(this.name, this.keys)
  }

  /**
   * This should be implemented in both Router and Method
   */
  add () {}

  /**
   * Finds request routes recursively
   */
  find () {}

  /**
   * Find the last available route for a given path
   *
   * @param {String} path
   * @param {String} method
   */
  findLastRoute (path, method) {
    const routeName = this.subCurrentRouteName(path)
    const nextPath = this.name === '/'
      ? path : path.substr(routeName.length)
    let route = null
    if (this.name === '/' && path === '/') {
      return {
        lastRoute: this,
        updatedPath: ''
      }
    }
    if (this.name === '/' || this.path.exec(routeName)) {
      route = this.children.find(route => {
        return route.path.exec(this.subCurrentRouteName(nextPath))
      })
    }
    return !route ? {
      lastRoute: this,
      updatedPath: nextPath
    } : route.findLastRoute(nextPath, method)
  }

  /**
   * Subtracting the first path name from the full path
   *
   * @param {String} path
   */
  subCurrentRouteName (path) {
    // if (path[0] !== '/') {
    //   path = '/' + path
    // }
    let routePath = ''
    for (let i = 0; i < path.length; i++) {
      routePath += path[i]
      if (path[i + 1] === '/') break
    }
    return routePath
  }
}

module.exports = Route
