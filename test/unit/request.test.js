const request = require('supertest')
const Qwark = require('../../src')

describe('Test request', () => {
  it('Hello World', async () => {
    const app = new Qwark()
    app.get('/', (req, res) => {
      res.end('Hello World')
    })
    expect((await request(app).get('/').send().then(res => res)).statusCode).toBe(200)
  })
})
