const Router = require('../../src/router')
const Method = require('../../src/router/method')

describe('Router constructor', () => {
  // Create router
  const router = new Router('/')

  it('Router name should be /', () => {
    expect(router.name).toBe('/')
  })
  it('Router children should be 0', () => {
    expect(router.children.length).toBe(0)
  })
  it('Router keys should be null', () => {
    expect(router.keys).toBe(null)
  })
  it('Router stack should be 0', () => {
    expect(router.stack.length).toBe(0)
  })
  describe('Check router path regex', () => {
    it('Path / should give array with /', () => {
      expect(router.path.exec('/')[0]).toBe('/')
    })
    it('This should return null', () => {
      expect(router.path.exec('/test')).toBe(null)
    })
  })
})

describe('Router tree using constructor', () => {
  let router = new Router('/funny/store')

  it('Router name should be /funny', () => {
    expect(router.name).toBe('/funny')
  })
  it('Router name should be /store', () => {
    expect(router.children[0].name).toBe('/store')
  })

  router = new Router('/funny/store/shopping/cart')

  it('Router name should be /funny', () => {
    expect(router.name).toBe('/funny')
  })
  it('Router name should be /store', () => {
    expect(router.children[0].name).toBe('/store')
  })
  it('Router name should be /shopping', () => {
    expect(router.children[0].children[0].name).toBe('/shopping')
  })
  it('Router name should be /cart', () => {
    expect(router.children[0].children[0].children[0].name).toBe('/cart')
  })
})

describe('Router tree using add method', () => {
  const router = new Router('/')

  router.add('/tree/method')

  it('Router name should be /', () => {
    expect(router.name).toBe('/')
  })

  it('Router name should be /tree', () => {
    expect(router.children[0].name).toBe('/tree')
  })

  it('Router name should be /method', () => {
    expect(router.children[0].children[0].name).toBe('/method')
  })
})

describe('Router find last route', () => {
  describe('Path part available', () => {
    const router = new Router('/hello/from/the/other/side')

    const { lastRoute, updatedPath } = router.findLastRoute('/hello/from/italy')
    it('The last route should be /from', () => {
      expect(lastRoute.name).toBe('/from')
    })

    it('updatedPath should be /italy', () => {
      expect(updatedPath).toBe('/italy')
    })
  })

  describe('No path parts available', () => {
    const router = new Router('/')
    router.add('/hello/from/france')

    const { lastRoute, updatedPath } = router.findLastRoute('/hi/from/italy')
    it('The last route should be /', () => {
      expect(lastRoute.name).toBe('/')
    })

    it('updatedPath should be /hi/from/italy', () => {
      expect(updatedPath).toBe('/hi/from/italy')
    })
  })
})

describe('Router tree using add method with method', () => {
  const router = new Router('/')

  router.add('/tree/method', 'get', (req, res) => {})

  it('Router name should be /', () => {
    expect(router.name).toBe('/')
  })

  it('Router name should be /tree', () => {
    expect(router.children[0].name).toBe('/tree')
  })

  it('Router name should be /method', () => {
    expect(router.children[0].children[0].name).toBe('/method')
  })

  it('Router name should be ', () => {
    expect(router.children[0].children[0].children[0].name).toBe('')
  })

  it('Router method should be get', () => {
    expect(router.children[0].children[0].children[0].method).toBe('get')
  })

  it('Router method should be get', () => {
    expect(router.children[0].children[0].children[0] instanceof Method).toBe(true)
  })
})

describe('Router find routes', () => {
  const router = new Router('/')

  router.get('/', (req, res) => 'Hello World')
  router.post('/send/hi', (req, res) => 'Hi Send')
  router.get('/say/hi', (req, res) => 'Hi')
  router.get('/say/hello', (req, res) => 'Hello')

  describe('Hello World', () => {
    const req = { method: 'get', routes: [], url: '/' }
    const isFound = router.find('/', req)
    it('Should be true', () => {
      expect(isFound).toBe(true)
    })

    it('Should return Hello World', () => {
      expect(req.routes[1].stack[0]()).toBe('Hello World')
    })
  })

  describe('Find hello', () => {
    const req = { method: 'get', routes: [], url: '/say/hello' }
    router.find('/say/hello', req)
    it('Should return 4 routes', () => {
      expect(req.routes.length).toBe(4)
    })
  })

  describe('Find hi', () => {
    const req = { method: 'GET', routes: [], url: '/say/hi' }
    router.find('/say/hi', req)
    it('Should route 4 should be method', () => {
      expect(req.routes[3] instanceof Method).toBe(true)
    })
  })

  describe('Find send hi ', () => {
    const req = { method: 'get', routes: [], url: '/send/hi' }
    const isFound = router.find('/send/hi', req)
    it('Should be false method (get)', () => {
      expect(isFound).toBe(false)
    })
  })

  describe('Find send hi ', () => {
    const req = { method: 'post', routes: [] }
    const isFound = router.find('/send/hi', req)
    it('Should be true method (post)', () => {
      expect(isFound).toBe(true)
    })
    it('Should be true method (post)', () => {
      expect(req.routes.length).toBe(4)
    })
  })
})
